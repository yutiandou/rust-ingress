# 编译
FROM rust:1.59 as build
WORKDIR /rust-ingress
COPY build.toml .cargo/config.toml
#COPY vendor vendor
#COPY Cargo.toml Cargo.lock ./
COPY . .
RUN cargo build --release --offline -q


FROM frolvlad/alpine-glibc:latest
#ENV RUST_LOG warn
EXPOSE 80
WORKDIR /root/
COPY --from=build /rust-ingress/target/release/rust-ingress .

CMD ["./rust-ingress", "run"]

#
#FROM frolvlad/alpine-glibc:latest
##ENV RUST_LOG warn
#EXPOSE 80
#WORKDIR /root/
##COPY --from=build /rust-ingress/target/release/rust-ingress .
#COPY ./target/release/rust-ingress .
#CMD ["./rust-ingress", "run"]

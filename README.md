# rust-ingress

> `rust-ingress` is rust implements ingress-controller
> and then we call it ring for short

## functions and plans
- basis
- [x] reverse proxy
- [x] dynamic routing
- [ ] scripting support
- [ ] management interface
- [ ] Extended middleware


- reverse proxy
- [x] http
- [x] http2
- [ ] https
- [ ] tcp
- [ ] grpc
- [ ] websocket


- routing and service discovery
- [x] k8s services
- [x] k8s ingress
- [ ] etcd
- [ ] consul


## build deploy and run

build ring image
```bash
cargo vendor
docker -t xxxx:v0.0.1 .
```
Author's mirror image`registry.cn-hangzhou.aliyuncs.com/wshr/rust-ingress:v0.1.3`

deploy image ring to your k8s cluster
refer to yamL in the deploy directory of this project
Configuration file Refer to the configuration file in the SRC /config directory of this project

**notice for config**
- Ingresslister.selector:
  Set the ingress that the service needs to care about,
  default:ingress-controller-app=rust-ingress
- K8s Services that create the specified label automatically creates the service entry point.
  It comes with a port 80 HTTP proxy entry point by default

# framework

![Architecture diagram](./docs/framework.jpg)

# author
- email: 1443965173@qq.com

# end
Stateless ingress implemented by pure Rust.
Unlike ingress-nginx, dynamic routing can be implemented without any restart.
And it will automatically manage the entry services according to the Services, without any configuration changes.
The current version is only an early version and more people are welcome to participate

纯rust实现的无状态的ingress。
区别于ingress-nginx，不需要任何重启既能实现动态路由。
并且会根据services自动管理入口服务，无需任何配置修改。
当前版本只是一个早期版本，欢迎更多的人参与进来。
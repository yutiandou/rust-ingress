#![allow(dead_code)]
use super::{HttpControlIpFilter, HttpControlRouter, HttpControlStop};
use crate::app::core::{ControlPanel, IpOps, OptResponse, RouterOps};
use crate::config::EntryPoint;
use anyhow::Result;
use std::sync::Arc;
use wd_run::Context;

pub struct HttpControlPanel {
    ep: Arc<EntryPoint>,
    ipc: Vec<Box<dyn HttpControlIpFilter>>,
    router: Vec<Box<dyn HttpControlRouter>>,
    stops: Vec<Box<dyn HttpControlStop>>,
}
impl HttpControlPanel {
    pub(crate) fn new(ep: EntryPoint) -> Self {
        let ep = Arc::new(ep);
        Self {
            ep,
            ipc: vec![],
            router: vec![],
            stops: vec![],
        }
    }
    pub fn register_ip_filter_control<T>(mut self, t: T) -> Self
    where
        T: HttpControlIpFilter + 'static,
    {
        self.ipc.push(Box::new(t));
        self
    }
    pub fn register_router_control<T>(mut self, t: T) -> Self
    where
        T: HttpControlRouter + 'static,
    {
        self.router.push(Box::new(t));
        self
    }
    pub fn register_stop_control<T>(mut self, t: T) -> Self
    where
        T: HttpControlStop + 'static,
    {
        self.stops.push(Box::new(t));
        self
    }
}

#[async_trait::async_trait]
impl ControlPanel for HttpControlPanel {
    // fn health(&mut self) -> Result<()> {
    //     todo!()
    // }

    async fn ip_filter(&mut self, ctx: Context, rp: Vec<IpOps>) -> Result<()> {
        for i in self.ipc.iter_mut() {
            i.ip_filter(ctx.clone(), rp.clone()).await?;
        }
        Ok(())
    }

    async fn router(&mut self, ctx: Context, ro: RouterOps) -> Result<Option<OptResponse>> {
        for i in self.router.iter_mut() {
            i.router(ctx.clone(), ro.clone()).await?;
        }
        Ok(None)
    }

    async fn stop(&mut self, o: Context) -> Result<Option<OptResponse>> {
        for i in self.stops.iter_mut() {
            let _ = i.stop(o.clone()).await;
        }
        Ok(None)
    }
    fn config(&mut self, _: Context) -> Arc<EntryPoint> {
        self.ep.clone()
    }
}

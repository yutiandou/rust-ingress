mod control;
mod middle_log;
mod middle_router;
mod module;
mod server;
mod util;

use crate::config::EntryPoint;
use anyhow::Result;
use control::HttpControlPanel;
use middle_log::HtpLogMiddle;
use middle_router::HtpRouterMiddle;
pub use module::*;
use server::*;
pub use util::*;

pub fn build_http_entry_point(ep: EntryPoint) -> Result<(HttpServer, HttpControlPanel)> {
    let (htp_mid_router, htp_ctl_router) = HtpRouterMiddle::new();
    let (ser, ser_stop) = HttpServer::new(ep.clone());
    let ser = ser
        .register_handle(HtpLogMiddle)
        .register_handle(htp_mid_router);
    let ctl = HttpControlPanel::new(ep)
        .register_router_control(htp_ctl_router)
        .register_stop_control(ser_stop);
    Ok((ser, ctl))
}

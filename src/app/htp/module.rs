use crate::app::core::{IpOps, OptResponse, RouterOps};
use anyhow::Result;
use hyper::{Body, Request, Response};
use wd_run::Context;

#[async_trait::async_trait]
pub trait HttpHandle: Sync + Send {
    async fn request(
        &self,
        _ctx: Context,
        req: Request<Body>,
    ) -> Result<Request<Body>, Response<Body>> {
        Ok(req)
    }
    async fn response(&self, _ctx: Context, resp: Response<Body>) -> Response<Body> {
        resp
    }
}

#[async_trait::async_trait]
pub trait HttpControlRouter: Sync + Send {
    async fn router(&mut self, _: Context, opera: RouterOps) -> Result<Option<OptResponse>>;
}

#[async_trait::async_trait]
pub trait HttpControlIpFilter: Sync + Send {
    async fn ip_filter(&mut self, _: Context, _: Vec<IpOps>) -> Result<()> {
        Ok(())
    }
}

#[async_trait::async_trait]
pub trait HttpControlStop: Sync + Send {
    async fn stop(&mut self, _: Context) -> Result<Option<OptResponse>>;
}

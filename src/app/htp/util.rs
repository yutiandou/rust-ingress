use hyper::{Request, Body};

pub fn get_host(req:&Request<Body>) ->String{
    let host = if let Some(host) = req.headers().get("Host") {
        if let Ok(s) = host.to_str() {
            s.to_string()
        } else {
            String::from("host not find")
        }
    } else {
        String::from("host not find")
    };
    if host.contains(":") {
        let mut ss:Vec<&str> = host.split(':').collect();
        return ss.remove(0).to_string();
    }
    return host
}
use std::future::Future;
use std::pin::Pin;
use wd_run::Context;

pub struct Exit {
    channel: (async_channel::Sender<()>, async_channel::Receiver<()>),
}
impl wd_run::EventHandle for Exit {
    fn handle(&self, ctx: Context) -> Pin<Box<dyn Future<Output = Context> + Send>> {
        let chan = self.channel.clone();
        Box::pin(async move {
            let _ = chan.0.send(()).await;
            tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;
            let _ =
                tokio::time::timeout(tokio::time::Duration::from_secs(180), chan.1.recv()).await;
            return ctx;
        })
    }
}
impl Exit {
    pub fn new(channel: (async_channel::Sender<()>, async_channel::Receiver<()>)) -> Self {
        Exit { channel }
    }
}

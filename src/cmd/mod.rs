mod exit;
mod run;

use exit::*;
use run::*;

pub async fn run_application() {
    let (r, e) = Run::new();
    wd_run::ArgsManager::new()
        .add_global_flag("name", "rust_ingress", "core name")
        .register_cmd(Run::cmd(), r)
        .register_exit(e)
        .run()
        .await;
}

mod config;

pub use config::*;
use std::path::Path;

pub fn load_config(path: impl AsRef<Path>) -> Config {
    wd_log::res_panic!(wd_run::load_config(path);"load config error")
}

#[cfg(test)]
mod test {
    #[test]
    fn test_load_config() {
        let config = super::load_config("./src/config/config.toml");
        wd_log::log_info_ln!("{}", config.json_show());
        assert_eq!(&config.server.release, &false, "")
    }
    #[test]
    #[should_panic]
    fn test_load_non_config() {
        let _config = super::load_config("./config.toml");
    }
}

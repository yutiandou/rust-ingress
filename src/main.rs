mod app;
mod cmd;
mod config;
mod infra;

use crate::cmd::run_application;

#[tokio::main]
async fn main() {
    run_application().await;
}
